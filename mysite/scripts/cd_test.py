import hashlib
import time
import urllib.request, json
from tabor.models import Turn, Member



def run():
    tm= time.time()
    tm_str=(tm.__str__())
    md5_str=hashlib.md5(('REFRESHES TO THE CORE' + tm.__str__()).encode()).hexdigest()
    key_str='turnusy'
    par_str='1'
    url_str='http://kubab.centrumdeti.cz/'+tm_str+'/' + md5_str + '/' +key_str+ '/' + par_str

    with urllib.request.urlopen(url_str) as url:
        data = json.loads(url.read().decode())
        print(data)
