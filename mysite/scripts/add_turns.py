from django.contrib.auth.models import User
from tabor.models import Turn, Member
import json
import hashlib
import time
import urllib.request

import unicodedata

def run():
    tm = time.time()
    tm_str = (tm.__str__())
    md5_str = hashlib.md5(('REFRESHES TO THE CORE' + tm.__str__()).encode()).hexdigest()
    key_str = 'turnusy'  ## 'deti', 'turnusy', 'detail', 'spolubydlici', 'tel'
    url_str = 'http://kubab.centrumdeti.cz/' + tm_str + '/' + md5_str + '/' + key_str
    print(url_str)
    url_foto = "http://ssl.centrumdeti.cz/dft/pidi/15013.jpg"
    vis=21-9

    with urllib.request.urlopen(url_str) as url:
        data = json.loads(url.read().decode())
        for turn_json in data['turnusy']:
            turn=Turn(  label = turn_json['TABOR'],
                        start = turn_json['TS_ZACATEK'],
                        end = turn_json['TS_KONEC'],
                        visible = vis < 0,
                        cd_id = int(turn_json['TS_ID'])
                    )
            print(turn.__str__())
            turn.save()
            vis-=1
