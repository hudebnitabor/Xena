from django.contrib.auth.models import User, Group
from tabor.models import Turn, Member, RegistrationForm
import json
import hashlib
import time
import urllib.request

import unicodedata


def rmdiacritics(char):
    '''
    Return the base character of char, by "removing" any
    diacritics like accents or curls and strokes and the like.
    '''

    desc = unicodedata.name(str(char))
    cutoff = desc.find(' WITH ')
    if cutoff != -1:
        desc = desc[:cutoff]
    return unicodedata.lookup(desc)

def remove_diacritic(name):
    out=''
    for char in name:
        out += rmdiacritics(char)

    return out

def name_sum(name):
    out=int(123)
    for ch in name:
        out += int(ord(ch))

    return out

def add_children(first_name,last_name,cd_id,turn):
    # createUser
    print('tardis create user')
    username=remove_diacritic(first_name+ '_' +last_name )
    username=username.lower()

    try:
        user = User.objects.get(username=username)
        user.first_name = first_name
        user.last_name = last_name
        user.save()

    except User.DoesNotExist:
        psw="Hrachov"+str(name_sum(username))
        user = User.objects.create_user(username, '', psw)
        user.first_name = first_name
        user.last_name = last_name
        user.save()

    try:
        user = User.objects.get(username=username)
        member = Member.objects.get(user=user)

    except Member.DoesNotExist:
        member = Member(user=user)
        member.save()

    try:
        rf = RegistrationForm.objects.get(cd_id=cd_id)
        rf.member = Member.objects.get(user=user)
        rf.turn=turn
        rf.save()
    except RegistrationForm.DoesNotExist:
        rf = RegistrationForm(member=Member.objects.get(user=user),turn=turn,cd_id=cd_id)
        rf.save()






def add_children_from_visible_turns():
    turns=Turn.objects.filter(visible=True)
    for turn in turns:
        tm = time.time()
        tm_str = (tm.__str__())
        md5_str = hashlib.md5(('REFRESHES TO THE CORE' + tm.__str__()).encode()).hexdigest()
        key_str = 'deti'  ## 'deti', 'turnusy', 'detail', 'spolubydlici', 'tel'
        par_str = turn.cd_id.__str__()
        url_str = 'http://kubab.centrumdeti.cz/' + tm_str + '/' + md5_str + '/' + key_str + '/' + par_str

        with urllib.request.urlopen(url_str) as url:
            print(url_str)
            data = json.loads(url.read().decode())
            for child in data['deti']:
                print('tardis')
                add_children(child['PA_JMENO'],child['PA_PRIJMENI'],child['PA_ID'],turn)



def add_instructors():
    #
    vedouci=Group.objects.get(name='Vedouci')
    print(vedouci.__str__())
    user = User.objects.create_user('Tomáš', '', 'raspberry314')
    # Update fields and then save again
    user.first_name = 'Tomáš'
    user.last_name = 'Stibor'
    user.groups.add(vedouci)
    user.save()

    user = User.objects.create_user('Dan', '', 'Jahudka69')
    # Update fields and then save again
    user.first_name = 'Dan'
    user.last_name = 'Kadera'
    user.groups.add(vedouci)
    user.save()

    user = User.objects.create_user('Jarda', '', 'Podoli69')
    # Update fields and then save again
    user.first_name = 'Jarda'
    user.last_name = 'Syrový'
    user.groups.add(vedouci)
    user.save()

    user = User.objects.create_user('Šplíchy', '', 'NesnasimMetal666')
    # Update fields and then save again
    user.first_name = 'Martin'
    user.last_name = 'Šplíchal'
    user.groups.add(vedouci)
    user.save()

    user = User.objects.create_user('Johanka', '', '666Stin666')
    # Update fields and then save again
    user.first_name = 'Johanka'
    user.last_name = 'Tichá'
    user.groups.add(vedouci)
    user.save()

    user = User.objects.create_user('Párek', '', 'EpicFail69')
    # Update fields and then save again
    user.first_name = 'Kuba'
    user.last_name = 'Vaněk'
    user.groups.add(vedouci)
    user.save()

    user = User.objects.create_user('Marťas', '', 'Beatles4ever')
    # Update fields and then save again
    user.first_name = 'Martin'
    user.last_name = 'Rufer'
    user.groups.add(vedouci)
    user.save()


    user = User.objects.create_user('Lucka', '', 'IAmGroot1')
    # Update fields and then save again
    user.first_name = 'Lucka'
    user.last_name = 'Fialová'
    user.groups.add(vedouci)
    user.save()


def run():
    #print('add_instructors')

    #add_instructors()
    print('add_children')

    add_children_from_visible_turns()


