


(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.select').material_select();

  }); // end of document ready
})(jQuery); // end of jQuery name space

$(function(){
  $(window).on('load resize', adjustIframe);
});

function adjustIframe() {
  $(parent.document.getElementById("responsive-iframe")).css("height", $("html").css("height"));
}
