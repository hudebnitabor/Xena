(function($) {
    $(function() {
        var flashes = $('.flash');

        window.setTimeout(function () {
            flashes.slideUp('slow');
        }, 8000)
    });

    window.expand_str = function expand_str(s) {
        var i = 0, args = arguments;

        return s.replace(/%/g, function () {
            if (++i >= args.length) {
                throw new Error('Cannot expand placeholder #' + i + ', too few arguments');
            }

            return args[i];
        });
    };

    window.flash_message = function flash_message(msg, type) {
        var elem = $('<div></div>');
        elem.addClass('flash flash-' + type);
        elem.text(msg);
        elem.css('display', 'none');
        elem.appendTo('#flashes');
        elem.slideDown('fast');

        window.setTimeout(function() {
            elem.slideUp('slow');
        }, 8000);
    };

    window.open_dialog = function open_dialog(id, setup) {
        var elem = $('#' + id),
            $d = $(document);

        if (!elem.hasClass('dialog-content')) {
            var holder = $('<div></div>')
                .addClass('dialog-holder')
                .appendTo('body');

            elem.addClass('dialog-content')
                .css('display', '')
                .appendTo(holder);

            setup && setup(elem, true);

        } else {
            setup && setup(elem);
        }

        window.setTimeout(function() {
            elem.parent().addClass('opened');
        }, 10);

        function close() {
            elem.parent().removeClass('opened');
            elem.off('.dialog');
            $d.off('.dialog');
        }

        return new Promise(function(fulfill, reject) {
            $d.on('keydown.dialog', function (evt) {
                if (evt.which === 13) {
                    close();
                    fulfill(elem);
                } else if (evt.which === 27) {
                    close();
                    reject(null);
                }
            });

            elem.on('click.dialog', 'a[data-action], button[data-action]', function (evt) {
                if (evt.isDefaultPrevented()) {
                    return;
                }

                evt.preventDefault();
                close();

                if (this.getAttribute('data-action') === 'fulfill') {
                    fulfill(elem);
                } else {
                    reject(this);
                }
            });
        });
    };
})(jQuery);
