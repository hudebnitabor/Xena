from django.contrib import admin
from .models import RegistrationForm, Turn, FCITeam,FCISite, FCICeleb,Band,Member,PayDay, PayDayTeamHistory, PayDayCelebHistory, BasicContent, IssueStatus, Issue, IssueType, RehearsalRoom

admin.site.register([Turn,FCITeam,FCISite,FCICeleb,PayDay, PayDayTeamHistory, PayDayCelebHistory,BasicContent,IssueStatus, Issue, IssueType,RehearsalRoom])

class MBInline(admin.TabularInline):
    model = Band.members.through

class MRFInline(admin.StackedInline):
    model = RegistrationForm
    extra = 20

class MemberAdmin(admin.ModelAdmin):
    inlines = [
        MBInline, MRFInline
    ]

admin.site.register(Member, MemberAdmin)


class BandAdmin(admin.ModelAdmin):
    inlines = [
        MBInline,
    ]
    exclude = ('members',)

admin.site.register(Band, BandAdmin)
