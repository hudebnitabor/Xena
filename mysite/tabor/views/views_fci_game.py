#QyD9G8h0wg


from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.models import Group

import datetime
import json
from tabor.models import  FCITeam, FCICeleb,Band,InitCondition,PayDay, BasicContent, Member, Turn, FCISite,Issue, IssueStatus,IssueType, PayDayTeamHistory, Rehearsal, RehearsalRoom
from django.core.urlresolvers import resolve
from .views_base import commonArgs


def fci_home_view(request, turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    args = commonArgs(request)
    fci_teams_at_turn = FCITeam.objects.filter(turn=sel_turn)
    args.update({'fci_teams': fci_teams_at_turn})
    celebs=FCICeleb.objects.filter(team__in=fci_teams_at_turn)
    args.update({'sites': FCISite.objects.all()})
    args.update({'celebs': celebs})
    args.update({'sel_turn': sel_turn})
    args.update({'jobless': celebs.filter(site=None)})
    pay_days = PayDay.objects.filter(turn=sel_turn)
    args.update({'pay_days': pay_days})
    return render(request, 'fci_home.html', args)



def employ(request, team_id):
    celeb = get_object_or_404(FCICeleb, id=int(request.POST['celeb_id']))
    site_id=int(request.POST['site_id'])


    if site_id!=-1:
        site = get_object_or_404(FCISite, id=int(site_id))
        vir = int(request.POST['viral_id'])
        celeb.mult_vir = vir
        celeb.site = site
    else:
        celeb.mult_vir = 0
        celeb.site = None

    celeb.save()
    return HttpResponseRedirect(reverse('tabor:fci_team', args=(team_id,)))


def fci_team_view(request, team_id):
    sel_team = FCITeam.objects.get(id=team_id)
    sel_turn = sel_team.turn
    celebs= FCICeleb.objects.filter(team=sel_team)
    args=commonArgs(request)
    args.update({'team': sel_team, 'celebs': celebs, 'sel_turn': sel_turn})
    args.update({'sites': FCISite.objects.all()})
    return render(request, 'fci_team.html', args)


def pay_days_view(request, turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    args = commonArgs(request)
    args.update({'sel_turn': sel_turn})
    pay_days=PayDay.objects.filter(turn=sel_turn)
    args.update({'pay_days': pay_days})
    return render(request, 'fci_pay_days.html', args)


def make_dough(sel_turn, user):

    celebs = FCICeleb.objects.all()

    for celeb in celebs:
        if celeb.team.turn == sel_turn:
            celeb.update()

    pay_day = PayDay(moment=datetime.datetime.now(), turn=sel_turn, user=user)
    pay_day.save()

    for celeb in celebs:
        if celeb.team.turn == sel_turn:
            celeb.backup(pay_day)

    teams = FCITeam.objects.filter(turn=sel_turn)
    for team in teams:
        team.backup(pay_day)


def pay_days_new(request, turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    make_dough(sel_turn=sel_turn, user=request.user.__str__())
    return HttpResponseRedirect(reverse('tabor:fci_home', args=(turn_id,)))


def pay_day_return(request,turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    pay_day=get_object_or_404(PayDay, id=int(request.POST['pay_day_id']))
    pay_day.restore()
    return HttpResponseRedirect(reverse('tabor:fci_home', args=(turn_id,)))

def game_settings_view(request, turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    args = commonArgs(request)
    args.update({'sel_turn': sel_turn})
    init_conds = InitCondition.objects.filter(turn=sel_turn)
    args.update({'init_conds': init_conds})

    ids = []
    for init_cond in init_conds:
        ids += [init_cond.celeb.id]

    pay_days = PayDay.objects.filter(turn=sel_turn)
    args.update({'pay_days': pay_days})
    celebs = FCICeleb.objects.exclude(id__in =ids)
    args.update({'celebs': celebs})
    teams = FCITeam.objects.filter(turn=sel_turn)
    args.update({'teams': teams})
    return render(request, 'fci_settings.html', args)


def game_start(request, turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    celebs = FCICeleb.objects.all()
    for celeb in celebs:
        celeb.reset()

    teams=FCITeam.objects.filter(turn=sel_turn)
    for team in teams:
        team.reset()

    init_conds=InitCondition.objects.filter(turn=sel_turn)
    for init_cond in init_conds:
        init_cond.apply()


    PayDay.objects.filter(turn=sel_turn).delete()
    return HttpResponseRedirect(reverse('tabor:fci_home', args=(turn_id,)))


def game_new_init_con(request, turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    celeb = get_object_or_404(FCICeleb, id=int(request.POST['celeb_id']))
    team = get_object_or_404(FCITeam, id=int(request.POST['team_id']))
    init_con=InitCondition(turn=sel_turn,team=team,celeb=celeb)
    init_con.save()
    return HttpResponseRedirect(reverse('tabor:fci_settings', args=(turn_id,)))




    return render(request, 'issue_list.html', args)


def fci_details_view(request,turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    teams=FCITeam.objects.filter(turn=sel_turn)
    pay_days = PayDay.objects.filter(turn=sel_turn).order_by('moment')
    pay_day_lines=[]
    for pay_day in pay_days:
        pay_day_line=[pay_day.moment.time]
        for tm in teams:
            history = PayDayTeamHistory.objects.filter(pay_day=pay_day)
            print(tm.money)
            team_history = history.filter(team=tm)
            money=team_history.get().money
            pay_day_line+=[money]

        pay_day_lines+=[pay_day_line]

    args = commonArgs(request)
    args.update({'sel_turn': sel_turn})
    args.update({'pay_day_lines': pay_day_lines})
    args.update({'teams': teams})
    return render(request, 'fci_details.html', args)


