#QyD9G8h0wg


from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
import datetime
from tabor.models import  FCITeam, FCICeleb,Band,InitCondition,PayDay, BasicContent, Member, Turn, FCISite,Issue, IssueStatus,IssueType, PayDayTeamHistory, Rehearsal, RehearsalRoom
from .views_base import commonArgs



def issue_list_new(request, turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    args = commonArgs(request)
    args.update({'sel_turn': sel_turn})
    typ = IssueType.objects.get(id=request.POST['type_id'])
    status = IssueStatus.objects.get(id=3)
    body = request.POST['notice']
    issue= Issue(author=request.user.__str__(), body=body, status=status, type=typ,moment=datetime.datetime.now())
    issue.save()
    return HttpResponseRedirect(reverse('tabor:issue_list', args=(turn_id,)))


def issue_change_status(request, turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    args = commonArgs(request)
    args.update({'sel_turn': sel_turn})
    issue= Issue.objects.get(id=request.POST['issue_id'])
    try:
        status = IssueStatus.objects.get(label=request.POST['status_label'])
        print(issue.__str__() + " status changed to " +status.__str__())
    except IssueStatus.DoesNotExist:
        print(issue.__str__() + " issue will be deleted")
        status = None

    issue.status=status
    issue.save()
    return HttpResponseRedirect(reverse('tabor:issue_list', args=(turn_id,)))


def issue_list_view(request, turn_id):

    issues = Issue.objects.all()

    if request.POST.get('filter_type_id', False):
        try:
            itype = IssueType.objects.get(id=request.POST['filter_type_id'])
            issues = issues.filter(type=itype)
        except IssueType.DoesNotExist:
            print("no type selected")

    if request.POST.get('filter_status_id', False):
        try:
            status = IssueStatus.objects.get(id=request.POST['filter_status_id'])
            print("filter status:" + status.label)
            issues = issues.filter(status=status)
        except IssueStatus.DoesNotExist:
            print("no status selected")

    sel_turn = Turn.objects.get(id=turn_id)
    args = commonArgs(request)
    args.update({'sel_turn': sel_turn})
    args.update({'issues': issues})
    issue_types = IssueType.objects.all
    args.update({'issue_types': issue_types})
    issue_statuses = IssueStatus.objects.all
    args.update({'issue_statuses': issue_statuses})


    return render(request, 'issue_list.html', args)


