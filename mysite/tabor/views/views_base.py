#QyD9G8h0wg


from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.models import Group

import datetime
import json
from tabor.models import  FCITeam, FCICeleb,Band,InitCondition,PayDay, BasicContent, Member, Turn, FCISite,Issue, IssueStatus,IssueType, PayDayTeamHistory, Rehearsal, RehearsalRoom
from django.core.urlresolvers import resolve


def commonArgs(request):
    turn_list = Turn.objects.filter(visible=True)
    current_url = resolve(request.path_info).url_name
    current_url_full =  "tabor:"+current_url
    vedouci= Group.objects.get(id=1)
    print(current_url_full)
    return {'turn_list': turn_list.order_by('start'),'user':request.user, 'current_url':current_url_full, 'vedouci': vedouci}



def destBoduProNebelvir_view(request,turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    bands_at_turn = Band.objects.filter(turn=sel_turn)
    args = commonArgs(request)
    args.update({'sel_turn': sel_turn})
    args.update({'bands': bands_at_turn.order_by('score').reverse()})
    return render(request, 'deset4neb.html', args)




def index(request):
    visible_turns=Turn.objects.filter(visible=1)
    if visible_turns.count() > 0:
        return HttpResponseRedirect(reverse('tabor:deset4neb', args=(visible_turns[0].pk,)))
    else:
        return HttpResponse("No visible turns in database")



def rate(request, band_id):
    band = get_object_or_404(Band, pk=band_id)
    band.score+= int(request.POST['points'])
    band.save()
    return HttpResponseRedirect(reverse('tabor:deset4neb', args=(band.turn.id,)))

def notice_board(request, turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    args = commonArgs(request)
    args.update({'sel_turn': sel_turn})
    notice_board = BasicContent.objects.get(title='Nástěnka')
    args.update({'notice_board': notice_board})
    return render(request, 'notice_board.html', args)


