#QyD9G8h0wg


from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
import json
from tabor.models import  Band, Member, Turn, Rehearsal, RehearsalRoom
from .views_base import commonArgs



def time_table_view(request,turn_id):
    sel_turn = Turn.objects.get(id=turn_id)
    args = commonArgs(request)
    args.update({'sel_turn': sel_turn})
    bands = Band.objects.filter(turn=sel_turn)
    args.update({'turns_bands': bands})

    try:
        member=Member.objects.get(user=request.user)
        args.update({'members_bands': member.band_set.all()})
        args.update({'member': member})
        print(member.band_set.all())
    except Member.DoesNotExist:
        args.update({'members_bands': []})
        args.update({'member': []})

    bands_data = {}
    for band in bands:
        bands_data.update((band.serializable_object()))

    bands_data=get_bands()
    args.update({'bands_data': bands_data})

    valid_times=[{"start": "09:30", "end": "12:00"}, {"start": "14:30", "end": "18:00"}, {"start": "19:30", "end": "21:30"}]
    args.update({'valid_times':json.dumps(valid_times)})

    rehearsals = Rehearsal.objects.all
    args.update({'rehearsals': rehearsals})
    rehearsal_rooms = RehearsalRoom.objects.all
    args.update({'rehearsal_rooms': rehearsal_rooms})


    return render(request, 'time_table.html', args)


def time_table_api(request,turn_id):
    return 0





def get_bands():
    json_data={"1": {"id": 1, "name": "Muller Mix", "rehearsal_room": "A", "is_project": 0,
           "members": {"20064": {"name": "Sára  Davisonová", "is_guest": 0},
                       "20255": {"name": "Anna Mullerová", "is_guest": 0},
                       "20256": {"name": "Jana Mullerová", "is_guest": 0},
                       "20882": {"name": "Daniel Kadera", "is_guest": 0}}},
     "2": {"id": 2, "name": "Druhá", "rehearsal_room": "B", "is_project": 0,
           "members": {"20647": {"name": "Hana Mikolášová", "is_guest": 0}, "20669": {"name": "Jan Eis", "is_guest": 0},
                       "20878": {"name": "Agáta Šarochová", "is_guest": 0}}},
     "3": {"id": 3, "name": "Implantáty vod Pantáty", "rehearsal_room": "F", "is_project": 0,
           "members": {"20356": {"name": "Lucie Landová", "is_guest": 0},
                       "20528": {"name": "Veronika Dibelková", "is_guest": 0}}},
     "4": {"id": 4, "name": "Brunejský sultán", "rehearsal_room": "G", "is_project": 0,
           "members": {"20064": {"name": "Sára  Davisonová", "is_guest": 0},
                       "20255": {"name": "Anna Mullerová", "is_guest": 0},
                       "20882": {"name": "Daniel Kadera", "is_guest": 0},
                       "22132": {"name": "David  Václavík", "is_guest": 0}}},
     "5": {"id": 5, "name": "Testovací Trio", "rehearsal_room": "F", "is_project": 0,
           "members": {"20064": {"name": "Sára  Davisonová", "is_guest": 0},
                       "20669": {"name": "Jan Eis", "is_guest": 0}, "20878": {"name": "Agáta Šarochová", "is_guest": 0},
                       "20916": {"name": "Jakub Bureš", "is_guest": 0}}}}
    return  json.dumps(json_data)



def time_table_add(request,turn_id):
    print("ouu jee")
    print(request.__str__())
    return HttpResponseRedirect(reverse('tabor:time_table', args=(turn_id,)))


def time_table_edit(request):
    print(request.__str__())

def time_table_delete(request):
    print(request.__str__())
