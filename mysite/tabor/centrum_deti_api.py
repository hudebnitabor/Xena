import json
import hashlib
import time
import urllib.request



def cd_api_get_data(key_str,par_str):
    tm = time.time()
    tm_str = (tm.__str__())
    md5_str = hashlib.md5(('REFRESHES TO THE CORE' + tm.__str__()).encode()).hexdigest()
    url_str = 'http://kubab.centrumdeti.cz/' + tm_str + '/' + md5_str + '/' + key_str + '/' + par_str
    url_foto = "http://ssl.centrumdeti.cz/dft/pidi/15013.jpg"
    data = None

    with urllib.request.urlopen(url_str) as url:
        data = json.loads(url.read().decode())

    return data

def cd_api_get_turnusy(par_str):
    par_str = str(par_str)
    key_str = 'turnusy'  ## 'deti', 'turnusy', 'detail', 'spolubydlici', 'tel'
    return cd_api_get_data(key_str,par_str)

def cd_api_deti(turn_id):
    par_str = str(turn_id)
    key_str = 'deti'  ## 'deti', 'turnusy', 'detail', 'spolubydlici', 'tel'
    return cd_api_get_data(key_str,par_str)

def cd_api_turn():
    par_str = ''
    key_str = 'turnusy'  ## 'deti', 'turnusy', 'detail', 'spolubydlici', 'tel'
    return cd_api_get_data(key_str,par_str)['turnusy']

def cd_api_detail(rf_id):
    par_str = str(rf_id)
    key_str = 'detail'  ## 'deti', 'turnusy', 'detail', 'spolubydlici', 'tel'
    return cd_api_get_data(key_str,par_str)['detail']

def cd_api_spolubydlici(turn_id):
    par_str = str(turn_id)
    key_str = 'spolubydlici'  ## 'deti', 'turnusy', 'detail', 'spolubydlici', 'tel'
    return cd_api_get_data(key_str,par_str)['spolubydlici']

def cd_api_tel(turn_id):
    par_str = str(turn_id)
    key_str = 'tel'  ## 'deti', 'turnusy', 'detail', 'spolubydlici', 'tel'
    return cd_api_get_data(key_str,par_str)['tel']
