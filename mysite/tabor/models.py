from django.contrib.auth.models import User
from django.db import models
from tinymce.models import HTMLField
from django.utils.translation import ugettext_lazy as _
import json
from .centrum_deti_api import cd_api_detail, cd_api_spolubydlici

import datetime
# Create your models here.


class BasicContent(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255,null=True, blank=True)
    body = HTMLField(_('Body'))

    def __str__(self):
        return self.title


class Turn(models.Model):
    label = models.CharField(max_length=100)
    start = models.DateField('turn start')
    end = models.DateField('turn end')
    visible = models.BooleanField()
    cd_id = models.IntegerField(unique=True)

    def __str__(self):
        return self.label + ' from ' + self.start.__str__() + ' to ' + self.end.__str__()




class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nick_name = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        if self.nick_name is not None:
            if not self.nick_name.isspace:
                return self.nick_name

        if self.user.first_name is not None:
            return self.user.first_name + " " + self.user.last_name

        return self.user.username

    def serializable_object(self):
        obj = {self.id.__str__(): {"id":self.id.__str__() , "name":self.__str__()}}
        return obj


class RegistrationForm(models.Model):
    cd_id = models.IntegerField(unique=True)
    turn = models.ForeignKey(Turn)
    member = models.ForeignKey(Member)

    def detail(self):
        return cd_api_detail(self.cd_id)


class MusicInstrument(models.Model):
    label = models.CharField(max_length=100)
    colour = models.CharField(max_length=32)
    members = models.ManyToManyField(Member)


class Band(models.Model):
    name = models.CharField(max_length=100)
    colour = models.CharField(max_length=32)
    turn = models.ForeignKey(Turn, on_delete=models.CASCADE)
    score = models.IntegerField()
    members = models.ManyToManyField(Member)

    def __str__(self):
        return self.name + '-' + self.turn.label

    def serialize_to_json(self):
        return json.dumps(self.serializable_object())

    def serializable_object(self):
        obj = {self.id.__str__(): {"id": self.id.__str__(), "name": self.name, 'members':[]}}
        for member in self.members.all():
            obj[self.id.__str__()]['members'].append(member.serializable_object())
        return obj





class RehearsalRoom(models.Model):
    name = models.CharField(max_length=100)
    colour = models.CharField(max_length=32)


class Rehearsal(models.Model):
    band = models.ForeignKey(Band, on_delete=models.CASCADE)
    room = models.ForeignKey(RehearsalRoom,  on_delete=models.CASCADE)
    date = models.DateField()
    starts_on = models.TimeField()
    ends_on = models.TimeField()


class FCITeam(models.Model):
    name = models.CharField(max_length=100)
    colour = models.CharField(max_length=32)
    money = models.IntegerField()
    turn = models.ForeignKey(Turn, on_delete=models.CASCADE)
    members = models.ManyToManyField(Member)

    def __str__(self):
        return self.name + '-' + self.turn.label

    def backup(self, pay_day):
        bckup = PayDayTeamHistory(team=self, money=self.money,pay_day=pay_day)
        bckup.save()

    def reset(self):
        self.money = 0
        self.save()


class FCISite(models.Model):
    name = models.CharField(max_length=100)
    story = models.CharField(max_length=2000)
    add_free = models.IntegerField()
    add_cool = models.IntegerField()
    add_in = models.IntegerField()

    def __str__(self):
        return self.name

    def update(self,celeb):
        celeb.mult_free +=self.add_free
        celeb.mult_cool += self.add_cool
        celeb.mult_in += self.add_in
        celeb.save()


class FCICeleb(models.Model):
    name = models.CharField(max_length=100)
    story = models.CharField(max_length=2000)
    mult_free = models.IntegerField()
    mult_cool = models.IntegerField()
    mult_in = models.IntegerField()
    mult_vir = models.IntegerField()
    site = models.ForeignKey(FCISite, on_delete=models.CASCADE,null=True,blank=True)
    team = models.ForeignKey(FCITeam, on_delete=models.CASCADE, null=True,blank=True)

    def __str__(self):
        return self.name

    def update(self):
        if self.site is not None:
            self.site.update(self)
            money = self.mult_free*self.mult_cool*self.mult_in*self.mult_vir
            team = self.team
            team.money += money
            team.save()

    def backup(self,pay_day):
        bckup =PayDayCelebHistory(celeb=self, mult_free=self.mult_free,
                                 mult_cool=self.mult_cool, mult_in=self.mult_in,
                                 mult_vir=self.mult_vir, site=self.site,
                                 pay_day=pay_day,team=self.team)
        bckup.save()

    def reset(self):
        self.team=None
        self.site=None
        self.mult_free = 5
        self.mult_cool = 5
        self.mult_in = 5
        self.mult_vir = 0
        self.save()




class PayDay(models.Model):
    moment = models.DateTimeField('Penize přišli:')
    turn = models.ForeignKey(Turn, on_delete=models.CASCADE)
    user = models.CharField(max_length=100)

    def restore(self):
        team_hs = PayDayTeamHistory.objects.filter(pay_day=self)
        for team_h in team_hs:
            team_h.restore()

        celeb_hs = PayDayCelebHistory.objects.filter(pay_day=self)
        for celeb_h in celeb_hs:
            celeb_h.restore()

        pay_days = PayDay.objects.filter(turn=self.turn)
        for pay_day in pay_days:
            if pay_day.moment > self.moment:
                pay_day.delete()

    def __str__(self):
        return self.moment.__str__() + " by " + self.user


class PayDayCelebHistory(models.Model):
    pay_day = models.ForeignKey(PayDay, on_delete=models.CASCADE)
    celeb = models.ForeignKey(FCICeleb, on_delete=models.CASCADE)
    site = models.ForeignKey(FCISite, on_delete=models.CASCADE,null=True,blank=True)
    team = models.ForeignKey(FCITeam, on_delete=models.CASCADE)
    mult_free = models.IntegerField()
    mult_cool = models.IntegerField()
    mult_in = models.IntegerField()
    mult_vir = models.IntegerField()

    def restore(self):
        celeb=self.celeb
        celeb.site=self.site
        celeb.team=self.team
        celeb.mult_free=self.mult_free
        celeb.mult_cool=self.mult_cool
        celeb.mult_in=self.mult_in
        celeb.mult_vir=self.mult_vir
        celeb.save()

class PayDayTeamHistory(models.Model):
    pay_day = models.ForeignKey(PayDay, on_delete=models.CASCADE)
    team = models.ForeignKey(FCITeam, on_delete=models.CASCADE)
    money = models.IntegerField()

    def restore(self):
        team=self.team
        team.money=self.money
        team.save()

class InitCondition(models.Model):
    turn = models.ForeignKey(Turn, on_delete=models.CASCADE)
    team = models.ForeignKey(FCITeam, on_delete=models.CASCADE)
    celeb = models.ForeignKey(FCICeleb, on_delete=models.CASCADE)

    def apply(self):
        self.celeb.team=self.team
        self.celeb.save()


class IssueStatus(models.Model):
    label = models.CharField(max_length=24)
    color = models.CharField(max_length=8)

    def __str__(self):
        return self.label


class IssueType(models.Model):
    label = models.CharField(max_length=24)
    icon = models.CharField(max_length=24)

    def __str__(self):
        return self.label


class Issue(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255, null=True, blank=True)
    body =  models.CharField(max_length=2048)
    moment = models.DateTimeField()
    status = models.ForeignKey(IssueStatus, on_delete=models.CASCADE, null=True, blank=True)
    type =models.ForeignKey(IssueType, on_delete=models.CASCADE,null=True, blank=True)

    def __str__(self):
        return self.title
