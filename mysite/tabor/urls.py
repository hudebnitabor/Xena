from django.conf.urls import url, include
from . import views

urlpatterns = [
    url('^$', views.index, name='index'),
    url('^deset4neb/(?P<turn_id>[0-9]+)$', views.destBoduProNebelvir_view, name='deset4neb'),
    url('^fci_home/(?P<turn_id>[0-9]+)$', views.fci_home_view, name='fci_home'),
    url('^notice_board/(?P<turn_id>[0-9]+)$', views.notice_board, name='notice_board'),
    url('^issue_list/(?P<turn_id>[0-9]+)$', views.issue_list_view, name='issue_list'),
    url('^issue_change_status/(?P<turn_id>[0-9]+)$', views.issue_change_status, name='issue_change_status'),
    url('^issue_list_new/(?P<turn_id>[0-9]+)$', views.issue_list_new, name='issue_list_new'),
    url('^fci_settings/(?P<turn_id>[0-9]+)$', views.game_settings_view, name='fci_settings'),
    url('^fci_details/(?P<turn_id>[0-9]+)$', views.fci_details_view, name='fci_details'),
    url('^new_init_con/(?P<turn_id>[0-9]+)$', views.game_new_init_con, name='new_init_con'),
    url('^restart/(?P<turn_id>[0-9]+)$', views.game_start, name='restart'),
    url('^return_to_day/(?P<turn_id>[0-9]+)$', views.pay_day_return, name='return_to_day'),
    url('^new_pay_day/(?P<turn_id>[0-9]+)$', views.pay_days_new, name='pay_days_new'),
    url('^fci_team/(?P<team_id>[0-9]+)$', views.fci_team_view, name='fci_team'),
    url('^accounts/', include('django.contrib.auth.urls')),
    url('^rate/(?P<band_id>[0-9]+)$',views.rate,name='rate'),
    url('^employ/(?P<team_id>[0-9]+)$',views.employ,name='employ'),
    url('^time_table/(?P<turn_id>[0-9]+)$', views.time_table_view, name='time_table'),
    url('^time_table/add/(?P<turn_id>[0-9]+)$', views.time_table_add, name='time_table_add'),
    url('^time_table/(?P<rehersal_id>[0-9]+)$/edit$', views.time_table_edit, name='time_table_edit'),
    url('^time_table/(?P<rehersal_id>[0-9]+)$/remove$', views.time_table_delete, name='time_table_delete'),


]

